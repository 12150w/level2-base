/*
 * JSON Interface
 *
 */
var PersistenceInterface = require('./persistence-interface');

module.exports = PersistenceInterface.extend({
	init: function(options) {
		this._options = options || {};
		
		// Merge defaults
		this._options.indent = this._options.indent || '    ';
	},
	
	serialize: function(data) {
		return JSON.stringify(data, null, this._options.indent);
	},
	
	parse: function(raw) {
		return JSON.parse(raw);
	}
});
