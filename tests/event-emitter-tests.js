/*
 * Tests the EventEmitter
 *
 */
var base = require('../src/index'),
	assert = require('assert');

describe('EventEmitter', function() {
	var TestEmitter = base.Class.extend(base.EventEmitter, {}),
		emitter;
	beforeEach('Create a new emitter implementation', function() {
		emitter = new TestEmitter();
	});

	describe('#on', function() {
		it('Adds an event listener', function(done) {
			this.timeout(250);
			
			emitter.on('test', function() {
				done();
			});
			
			emitter.emit('test');
		});
	});
	
	describe('#emit', function() {
		it('Passes the data to the handlers', function(done) {
			this.timeout(250);
			var testData1 = {},
				testData2 = {};
			
			emitter.on('test', function(passed1, passed2) {
				assert.ok(passed1 === testData1);
				assert.ok(passed2 === testData2)
				done();
			});
			
			emitter.emit('test', testData1, testData2);
		});
	});
});
