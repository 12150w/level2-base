/*
 * Error
 *   Extension of javascript error
 *
 */
var Class = require('./class');

module.exports = Class.extendGeneric(Error, {
    init: function(message) {
        Error.apply(this, arguments);
        Error.captureStackTrace(this, arguments.callee);
        
        this.message = message;
    }
});
