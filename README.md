# Level2 Base
Provides the base class and other bits of code that were being re-used within level2 projects.



## Class
The `Class` class provides a simple way of defining new classes and extending classes.
All uses of `Class` must extend `Class` itself or one of its subclasses.

```javascript
var Something = Class.extend({
	// Provide prototype here
});

var ChildSomething = Something.extend({
	// Override Something prototoype if needed
});
```

:information_source: Within override methods a special function, `this._super`, is available
	to call the super class's function (note that this is only defined in the child function
	and is removed after the method returns)

To provide a constructor use the `init` property of the prototype.
This function will be called when an instance is constructed.

```javascript
var Something = Class.extend({
	init: function() {
		// Do initialization here
	}
});
```

Mixins are also supported when using `Class.extend()`.

```javascript
var Something = Class.extend(Mixin1, Mixin2, {
	// Provide prototype here
});
```

### Class.extend([mixins...], prototype) - `Function`
Extends a `Class` with a new prototype.
If any mixins are provided their attributes will be copied to the new class's prototype.

* **[mixins...]** `Function`: mixins to apply to this class
* **prototype** `Object`: prototype to apply to this class

### Class.extendGeneric(baseType, [mixins...], prototype) - `Function`
Extends any type, even if that type was not created from `Class.extend()`.

* **baseType** `Function`: the base type to extend
* **[mixins...]** `Function`: mixins to apply to the extended class
* **prototype** `Object`: prototype to apply to the extended class



## Error
Built in extension of the javascript `Error` class which includes an `extend()` method.
Really this is just a convenience to calling `Class.extendGeneric(Error)`.

### new Error(message)
Constructs a new error message

* **message** `String`: The error's message

:warning: The stack will be traced up to the line where the error was constructed, not where it is thrown.
	(throwing errors in the same line they are constructed minimizes this issue).



## EventEmitter
Because it was used so often a simple event emitter class is provided.
This is intended to be used as a mixin.

### EventEmitter.on(eventName, handler)
Adds an event listener for the given event name.

* **eventName** `String`: The event name that the listener is waiting for
* **handler** `Function`: The event handler function

### EventEmitter.emit(eventName, [data...])
Calls all the registered event listeners for the given event.

* **eventName** `String`: The event to fire
* **[data...]** `Mixed`: The arguments to pass to the event handlers



## Settings
Provides an interface to cache persistent data to a data file.
To create a new settings file simply instantiate a Settings instance
	and pass the path to the file you want to create.

```javascript
var newSettings = new Settings('/path/to/settings-file.json');
newSettings.set('someKey', 'someValue');
```

:warning: The settings file won't be created until `Settings.set()` is
	called for the first time;

To retrieve settings values use the `Settings.get()` method.

```javascript
var sett = new Settings('/path/to/settings-file.json');
sett.get('some.value').then(function(someValue) {
	console.log(someValue);
});
```

Note that keys containing dots (`'.'`) will enter into objects.
If the base object does not exists `undefined` will be returned as
	the settings value.

#### new Settings(filePath, [dataInterface])
Creates a new Settings instance backed by the settings file

* **filePath** `String`: The path to this settings storage file
* **[dataInterface]** `PersistenceInterface`: The optional interface used for
	serializing and de-serializing the settings data (defaults to the provided
	`JSONInterface`)

#### Settings.get(key) - `Promise - Object`
Get the data at the key

* **key** `String`: The key for the data to get

:arrow_right: Returns a promise for the setting value

#### Settings.set(key, data) - `Promise`
Sets the data at the given key

* **key** `String`: Key of the setting to set
* **data** `Any`: The data to store (must
	be JSON serializable)

:arrow_right: Returns a promise that resolves when data is stored

#### Settings.getFilePath() - `String`
Get the file path used during initialization

:arrow_right: Returns the settings path



## PersistenceInterface
A `PersistenceInterface` implementation provides a way to serialize and
	de-serialize data in a generic fashion.
Originally this was added for use with the `Settings` class.

### PersistenceInterface.serialize(data) - `String`
Returns the string representation of the data

* **data** `Object`: The data to serialize

:arrow_right: Returns the serialized data

### PersistenceInterface.parse(raw) - `Object`
Returns the parsed data

* **raw** `String`: The data to be parsed

:arrow_right: Returns the parsed data



## JSONInterface
An implementation of `PersistenceInterface` using the JSON specification.

### new JSONInterface([options])
Create an interface using optional options

* **[options]** `Object`: Objects for the interface
	* **[indent]** `String`: The indent to use when serializing (default `'    '`, 4 spaces)
