/*
 * Persistence Interface
 *
 */
var Class = require('./class');

module.exports = Class.extend({
	serialize: function(data) {
		throw new Error('PersistenceInterface.serialize is not implemented');
	},
	
	parse: function(raw) {
		throw new Error('PersistenceInterface.parse is not implemented');
	}
});
