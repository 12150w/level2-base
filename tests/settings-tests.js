/* 
 * Tests the Settings class
 * 
 */
var base = require('../src/index'),
	path = require('path'),
	fs = require('fs'),
	os = require('os'),
	assert = require('assert');

describe('Settings', function() {
	var TEST_SETTINGS_FILE = path.join(os.tmpdir(), '.unit-test-settings'),
		testSettings;
	
	beforeEach('Create the settings instance', function() {
		testSettings = new base.Settings(TEST_SETTINGS_FILE);
	});
	afterEach('Destroy the settings and data', function(done) {
		fs.unlink(testSettings.getFilePath(), function(err) {
			if(!!err && err.code === 'ENOENT') done();
			else done(err);
		});
		delete testSettings;
	});
	
	describe('#set', function() {
		it('Stores the setting in the cache', function(done) {
			testSettings.set('a', 1).then(function() {
				assert.equal(testSettings._cache['a'], 1, 'The cache is not updated');
				done();
			}).done();
		});
		it('Persists the setting', function(done) {
			testSettings.set('a', 1).then(function() {
				var loadSettings = new base.Settings(TEST_SETTINGS_FILE);
				return loadSettings.get('a');
			}).then(function(readValue) {
				assert.equal(readValue, 1, 'The persisted value does not match');
				done();
			}).done();
		});
		it('Allows for setting compound paths', function(done) {
			testSettings.set('test.a', 1).then(function() {
				return testSettings.set('test.b', 2);
			}).then(function() {
				return testSettings.get('test');
			}).then(function(setVal) {
				assert.ok(setVal.a === 1);
				assert.ok(setVal.b === 2);
				
				done();
			}).done();
		});
	});
	
	describe('#get', function() {
		it('Allows for reading compound paths', function(done) {
			testSettings.set('test', {a: 1, b: 2}).then(function() {
				return testSettings.get('test.a');
			}).then(function(readA) {
				assert.ok(readA === 1);
				return testSettings.get('test.b');
			}).then(function(readB) {
				assert.ok(readB === 2);
				
				done();
			}).done();
		});
	});
});