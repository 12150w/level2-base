/* 
 * Settings
 *    Stores persistent data
 * 
 */
var Class = require('./class'),
	JSONInterface = require('./json-interface')
	q = require('q'),
	fs = require('fs');

module.exports = Class.extend({
	
	init: function(filePath, dataInterface) {
		this._filepath = filePath;
		this._dataInterface = dataInterface || new JSONInterface(); 
		
		this._cache = {};
		this._loaded = false;
	},
	
	get: function(path) {
		var self = this;
		
		return self._verifyLoaded().then(function() {
			return self._retrievePath(path);
		});
	},
	
	set: function(path, value) {
		var self = this;
		
		return self._verifyLoaded().then(function() {
			// Merge into the cache
			var setProblem = self._injectPath(path, value);
			if(setProblem !== null) throw new Error(setProblem);
			
			return q.nfcall(fs.writeFile, self._filepath, self._serialize(), {encoding: 'utf8'});
		});
	},
	
	reload: function() {
		var self = this;
		
		return q.ninvoke(fs, 'readFile', self._filepath, {encoding: 'utf8'}).fail(function(err) {
			// Handle not found error
			if(err.code !== 'ENOENT') throw err;
			
		}).then(function(raw) {
			// Deserialize the cache
			self._cache = self._deserialize(raw);
			if(self._cache == null) self._cache = {};
			self._loaded = true;
			
		});
	},
	
	getFilePath: function() {
		return this._filepath;
	},
	
	_injectPath: function(path, value) {
		if(path === null || path === undefined || path === NaN) return 'Path can not be null';
		if(typeof path !== 'string') return 'Path must be a String';
		
		var currentNode = this._cache,
			nodePaths = path.split('.'),
			_i, path;
		
		for(_i=0; _i<nodePaths.length; _i++) {
			path = nodePaths[_i];
			
			if(_i === nodePaths.length - 1) {
				currentNode[path] = value;
				break;
			}
			
			if(typeof currentNode[path] !== 'object') {
				currentNode[path] = {}
			}
			currentNode = currentNode[path];
		}
		
		return null;
	},
	
	_retrievePath: function(path) {
		if(path === null || path === undefined || path === NaN) throw new Error('Path can not be null');
		if(typeof path !== 'string') throw new Error('Path must be a String');
		
		var currentNode = this._cache,
			nodePaths = path.split('.'),
			found = true,
			_i, path, value;
		
		for(_i=0; _i<nodePaths.length; _i++) {
			path = nodePaths[_i];
			
			if(_i === nodePaths.length - 1) {
				value = currentNode[path];
				break;
			}
			
			if(typeof currentNode[path] !== 'object') {
				found = false;
				break;
			}
			currentNode = currentNode[path];
		}
		
		if(found === false) return undefined;
		return value;
	},
	
	_serialize: function() {
		return this._dataInterface.serialize(this._cache);
	},
	
	_deserialize: function(raw) {
		return this._dataInterface.parse(raw || '{}');
	},
	
	_verifyLoaded: function() {
		if(this._loaded !== true) return this.reload();
		else return q();
	}
});