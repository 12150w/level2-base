/* 
 * Tests the Error
 *
 */
var base = require('../src/index'),
	assert = require('assert');

describe('Error', function() {
	it('Extends the javascript Error', function() {
		var TestError = base.Error.extend({}),
			err = new TestError('This is a test error');
		
		assert.ok(err instanceof Error);
	});
});
