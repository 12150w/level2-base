/*
 * Event Emitter Mixin
 *
 */
var Class = require('./class');

var EventEmitter = Class.extend({
	on: function(eventName, handler) {
		var eventMap = this._getEventMap();
		
		// Add event to even map if needed
		if(!eventMap[eventName]) {
			eventMap[eventName] = [];
		}
		
		if(typeof(handler) === 'function') {
			eventMap[eventName].push(handler);
		}
	},
	
	emit: function(eventName) {
		var eventMap = this._getEventMap(),
			handlers = eventMap[eventName] || [];
		
		// Extract data to pass to handlers
		var handlerData = [];
		for(var i=0; i<arguments.length; i++) {
			if(i === 0) continue;
			handlerData.push(arguments[i]);
		}
		
		for(var i=0; i<handlers.length; i++) {
			handlers[i].apply(this, handlerData);
		}
	},
	
	_getEventMap: function() {
		if(this._eventMap == null) {
			this._eventMap = {};
		}
		
		return this._eventMap;
	}
});

module.exports = EventEmitter;
