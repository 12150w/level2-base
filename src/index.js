/*
 * Base API
 *
 */
module.exports = {
	Class: require('./class'),
	Error: require('./error'),
	
	EventEmitter: require('./event-emitter'),
	
	Settings: require('./settings'),
	PersistenceInterface: require('./persistence-interface'),
	JSONInterface: require('./json-interface')
};
